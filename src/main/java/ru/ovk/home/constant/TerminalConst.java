package ru.ovk.home.constant;

public class TerminalConst {
    public static final String CMD_HELP      ="help";
    public static final String CMD_VERSION   ="version";
    public static final String CMD_ERROR     ="error";
    public static final String CMD_ABOUT     ="about";
    public static final String CMD_DEFAULT   ="default";
    public static final String CMD_EXIT      ="exit";

    public static final String PROJECT_CREATE ="project-create";
    public static final String PROJECT_CLEAR  ="project-clear";
    public static final String PROJECT_LIST   ="project-list";

    public static final String TASK_CREATE ="task-create";
    public static final String TASK_CLEAR  ="task-clear";
    public static final String TASK_LIST   ="task-list";




    public static final String TELL_HELP     ="помощь";
    public static final String TELL_VERSION  ="версия";
    public static final String TELL_ERROR    ="параметр отсутствует";
    public static final String TELL_ABOUT    ="о программе";
    public static final String TELL_DEFAULT  ="по умолчанию";
    public static final String TELL_EXIT     ="выход";
};



