package ru.ovk.home;

import ru.ovk.home.dao.ProjectDAO;
import ru.ovk.home.dao.TaskDAO;

import java.util.Arrays;
import java.util.Scanner;

import static ru.ovk.home.constant.TerminalConst.*;

public class Main {
    private static final ProjectDAO projectDAO = new ProjectDAO();
    private static final TaskDAO    taskDAO    = new TaskDAO();
    private static final Scanner    scaner     = new Scanner(System.in);

    public static void main(String[] args) {
        DisplayTerminalMess(args);

        String command = "";
        while (!CMD_EXIT.equals(command)) {
            command = scaner.nextLine();
            DisplayTerminalMess(command);
        }
    }

    private static void DisplayTerminalMess(final String[] args) {
        String sCommand;
        if (args.length < 1) sCommand = "default";
        else sCommand = args[0];
        final int result = DisplayTerminalMess(sCommand);
       // System.exit(result);
    }

    private static int DisplayTerminalMess(final String sCommand) {
        switch (sCommand) {
            case CMD_ABOUT:      return displayAbout();
            case CMD_VERSION:    return displayVersion();
            case CMD_HELP:       return displayHelp();
            case CMD_DEFAULT:    return displayDefault();
            case CMD_EXIT:       return displayExit();

            case PROJECT_CLEAR:  return clearProject();
            case PROJECT_CREATE: return createProject();
            case PROJECT_LIST:   return listProject();

            case TASK_CLEAR:     return clearTask();
            case TASK_CREATE:    return createTask();
            case TASK_LIST:      return listTask();

            default:             return displayErr();

        }
    }

    private static int displayVersion() {
        System.out.println(TELL_VERSION);
        return 0;
    }

    private static int displayAbout() {
        System.out.println(TELL_ABOUT);
        return 0;
    }

    private static int displayHelp() {
        System.out.println("\"project-create\"   create new project");
        System.out.println("\"project-list\"     list projects");
        System.out.println("\"project-clear\"    clear projects");
        System.out.println("\"task-create\"   create new task");
        System.out.println("\"task-list\"     list tasks");
        System.out.println("\"task-clear\"    clear tasks");
        System.out.println("\"exit\"    exit from programm");

        return 0;
    }

    private static int displayDefault() {
        System.out.println("Welcome");
        return 0;
    }

    private static int displayErr() {
        System.out.println(TELL_ERROR);
        return -1;
    }

    private static int displayExit() {
        System.out.println(TELL_EXIT);
        return 0;
    }

    private static int createProject() {
        System.out.println("[Create project]");
        System.out.println("inpunt project name");
        final String name = scaner.nextLine();
        projectDAO.create(name);
        System.out.println("[OK]");
        return 0;
    }

    private static int clearProject() {
        System.out.println("[Clear project]");
        projectDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static int listProject() {
        System.out.println("[List project]");
        System.out.println(projectDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }

    private static int createTask() {
        System.out.println("[Create Task]");
        System.out.println("inpunt task name");
        final String name = scaner.nextLine();
        taskDAO.create(name);

        System.out.println("[OK]");
        return 0;
    }

    private static int clearTask() {
        System.out.println("[Clear Task]");
        taskDAO.clear();
        System.out.println("[OK]");
        return 0;
    }

    private static int listTask() {
        System.out.println("[List Task]");
        System.out.println(taskDAO.findAll());
        System.out.println("[OK]");
        return 0;
    }
}